This is the code of 1A2B game.
It was my homework of experiment physic course but I did not finish it.
Therefore, I do it agian.

Game Rule:
1. Think a  a 4-digit number in your mind.
2. This program guesses the number and you have to reply ?A?B.
    A: number is right and position is matched.
    B: number is right but position is unmatched.
3. When computer guess number correctly (get 4A), this game ends.